<?php
	//Create body of page
	class Body
	{
		private $src = "";
		function Body($src)
		{
			$this->src = $src;
		}
		//render src of page
		public function render()
		{
?>
			<div id="body">
				<div id="bodyBox" class="bodyText">
					<?php echo $this->src; ?>
				</div>
			</div>
<?php
		}
	}
?>