<?php
	class Login
	{
		private $SQL_TABLE = null;
		private $Auth = null;
		private $link = null;
		function Login($SQL_TABLE, $Auth, $link)
		{
			$this->SQL_TABLE = $SQL_TABLE;
			$this->Auth = $Auth;
			$this->link = $link;
		}
		function render()
		{
			if($this->Auth->isAuthenticated())
				header("location: ".$this->link);
?>
		<div id="login">
			<div id="logindiv" class="center">
				<span class="center nopadding">Enter Credentials</span></br></br>
				<span class="left nopadding">Username: </span><input type="text" id="txt_user" value=""/></br>
				<span class="left nopadding">Password: </span><input type="password" id="txt_pass" value=""/></br></br>
				<div id="lWarning"></div>
				<span class="center nopadding"><input type="button" id="btn_login" value="Login" /></span>
			</div>
		</div>
		<script>
			$("#btn_login").click(login);
			function login()
			{
				$.post("./Authentication.php",
				{
				  user: $("#txt_user").val(),
				  pass: $("#txt_pass").val()
				},
				function(data,status){
					$("#lWarning").html(data);
					var obj = JSON.parse(data);
					if(obj==false)
						$("#lWarning").html("Invalid user name or password");
					else
					{
						$("#lWarning").html("");
						window.location.href = "<?php echo $this->link; ?>";
					}
				});
			}
			function onEnter(e)
			{
				if(e.keyCode == 13)
				{
					login();
				}
			}
			$('#txt_pass').keyup(onEnter);
			$('#txt_user').keyup(onEnter);
		</script>
<?php
		}
	}
?>