<?php
	//Create a header
	class Header
	{
		private $title = "";
		private $logo = "";
		private $Auth = "";
		private $link = "";
		public function Header($title, $logo, $Auth, $link)
		{
			$this->title = $title;
			$this->logo = $logo;
			$this->Auth = $Auth;
			$this->link = $link;
		}
		public function render()
		{
?>
			<div id="header" class="center">
				<img id="headerLogo" src="<?php echo $this->logo;?>"/>
				<div id="headerTitle"><?php echo $this->title;?></div>
				<div id="headerUser">
					<ul>
						<li>
<?php
							if($this->Auth->isAuthenticated())
							{
								echo $this->Auth->getUser();
?>					
								<ul>
									<?php if($this->Auth->hasPermissions(AUTH_LEVEL_WRITER)) { ?>
										<li><a href="admin.php">Admin</a></li>
									<?php } ?>
									<li><a href="<?php echo ($this->link."?id=-3") ?>">Log Out</a></li>
								</ul>						
<?php
							}
							else
							{
?>
								<a href="<?php echo ($this->link."?id=-2") ?>">Log In</a>
<?php
							}
?>
						</li>
					</ul>
				</div>
			</div>
<?php
		}
	}
?>