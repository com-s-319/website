<?php
	//Create a navigation menu
	class Nav
	{
		private $SQL_TABLE = null;
		private $s_id = null;
		private $link = null;
		function Nav($SQL_TABLE, $link, $s_id)
		{
			$this->SQL_TABLE = $SQL_TABLE;
			$this->link = $link;
			$this->s_id = $s_id;
		}
		
		//recursive function to build menu hierarchy
		private function createLevel($level)
		{
			//Base case: if the level is empty
			if(!isset($level))
				return "";
			//Create an unordered list for the level
			$navbar = "\r\n<ul>\r\n";
			//populate list with elements
			foreach($level as $page)
			{
				$id=$page->GET_ATTRIBUTE(0);
				//Highlight the selected page.
				if($id==$this->s_id)
					$linkType="NavSelected";
				else
					$linkType="NavLink";
				$title=$page->GET_ATTRIBUTE(2);
				//Add to navbar
				$navbar .= "<li><a class='".$linkType."' href='".$this->link."?id=".$id."'>".$title."</a>";
				if($id != 0)
					$navbar  .= $this->createLevel($this->SQL_TABLE->GET_ROWS_BY_ATTRIBUTE(1,$id));
				$navbar  .= "</li>\r\n";
			}
			$navbar .= "</ul>\r\n";
			return $navbar;
		}
		//Render navigation menu
		public function render()
		{
?>
			<nav id="navBar">
				<?php 
					$level = $this->SQL_TABLE->GET_ROWS_BY_ATTRIBUTE(1,0);
					//Add Home link if specified
					if(true)
						array_unshift($level, $this->SQL_TABLE->GET_ROW(0));
					echo $this->createLevel($level);
				?>
			</nav>
<?php
		}
	}
?>