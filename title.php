<?php
	//Create page title
	class Title
	{
		private $id = 0;
		private $title = "";
		private $desc = null;
		private $img = null;
		function Title($id, $title, $desc=null, $img=null)
		{
			$this->id = $id;
			$this->title = $title;
			$this->desc = $desc;
			$this->img = $img;
		}
		
		//render title
		function render()
		{	
			//Case where title image is defined.
			if($this->img != null)
			{
?>
				<div id="Title">
					<div id="TitleImg" <?php echo "style=\"background-image: url('".$this->img."');\"";?>>
						<div id="TitleBox">
							<p id="TitleText"><?php echo $this->title;?></p>
							<p id="TitleDesc"><?php echo $this->desc;?></p>
						</div>
					</div>
				</div>
<?php
			}
			//Case where image is not defined.
			else
			{
?>
				<div id="Title">
					<div id="TitleOnly">
						<p id="TitleText"><?php echo $this->title;?></p>
						<?php if($this->desc != null) { ?>
							<p id="TitleDesc"><?php echo $this->desc;?></p>
						<?php } ?>
					</div>
				</div>
<?php
			}
		}
	}
?>