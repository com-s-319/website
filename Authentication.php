<?php
	define("AUTH_LEVEL_ADMIN"		, 4);
	define("AUTH_LEVEL_DESIGNER"	, 3);
	define("AUTH_LEVEL_PUBLIShER"	, 2);
	define("AUTH_LEVEL_WRITER"		, 1);
	define("AUTH_LEVEL_NONE"		, 0);
	session_start();
	if(isset($_POST["user"]))
	{
		include "./SQL.php";
		$SQL = new SQL();
		$Auth = new Authentication($SQL->GET_TABLE(1));
		$Auth->Authenticate($_POST["user"], $_POST["pass"]);
		echo json_encode($Auth->isAuthenticated());
	}
	class Authentication
	{
		private $SQL_TABLE = null;
		function Authentication($SQL_TABLE)
		{
			$this->SQL_TABLE = $SQL_TABLE;
		}
		function isAuthenticated()
		{
			if(isset($_SESSION['isAUTHENTICATED']))
				return $_SESSION['isAUTHENTICATED'];
			return false;
		}
		//return true if user has the specified permission level or higher
		function hasPermissions($permissions)
		{
			if($permissions <= $this->getPermissions())
				return true;
			return false;
		}
		//return number signifying permissions
		function getPermissions()
		{
			if(!$this->isAuthenticated())
				return AUTH_LEVEL_NONE;
			return $this->SQL_TABLE->GET_FIRST_ROW_BY_ATTRIBUTE(0,$_SESSION["AUTH_User"])->GET_ATTRIBUTE(2);
		}
		
		function Authenticate($user, $pass)
		{
			foreach($this->SQL_TABLE->GET_ROWS() as $candidate)
			{
				if(strtolower($candidate->GET_ATTRIBUTE(0)) == strtolower($user))
				{
					$_SESSION["AUTH_User"] = $candidate->GET_ATTRIBUTE(0);
					if($candidate->GET_ATTRIBUTE(1) == hash('sha256',$pass))
						$_SESSION['isAUTHENTICATED'] = true;
					else
						$_SESSION["isAUTHENTICATED"] = false;
					return;
				}
			}
		}
		function TerminateUser()
		{
			session_destroy();
			header("location: ?id=-2");
		}
		function getUser()
		{
			return $_SESSION['AUTH_User'];
		}
	}
?>