<?php
	//Create recursive list of links to current page and all parents until the HOME page (0) is reached
	class Breadcrumb
	{
		private $SQL_TABLE = null;
		private $link = null;
		private $id = null;
		private $title = "";
		//$title is needed as special pages cannot be accessed from SQL database.
		function Breadcrumb($SQL_TABLE, $link, $id, $title)
		{
			$this->SQL_TABLE = $SQL_TABLE;
			$this->link = $link;
			$this->id = $id;
			$this->title = $title;
		}
		public function render()
		{
			//Create Link to current Page
			$t_id = $this->id;
			$t_title = $this->title;
			$crumb = "<a class='breadcrumbLink' href='".$this->link."?id=".$t_id."'>".$t_title."</a>";
			//Get Parent ID
			$t_id = $this->SQL_TABLE->GET_ROW($t_id)->GET_ATTRIBUTE(1);
			//If parent doesn't exist, end generation
			while(($t_id !== -1)&&($t_id!==null))
			{
				//If parent exists, add to breadcrumb
				$t_title = $this->SQL_TABLE->GET_ROW($t_id)->GET_ATTRIBUTE(2);
				$crumb = "<a class='breadcrumbLink' href='".$this->link."?id=".$t_id."'>".$t_title."</a> > ".$crumb;
				//Get parent id
				$t_id = $this->SQL_TABLE->GET_ROW($t_id)->GET_ATTRIBUTE(1);
			}
?>
			<div id="breadcrumb">
				<div id="breadcrumbBox">
					<div id="breadcrumbText">
						<?php echo $crumb;?>
					</div>
				</div>
			</div>
<?php
		}
	}
?>
