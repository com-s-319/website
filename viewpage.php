<?php
	//Include all components
	include "./Authentication.php";
	include "./SQL.php";

	include "./css.php";
	include "./header.php";
	include "./nav.php";
	include "./title.php";
	include "./breadcrumb.php";
	include "./body.php";
	include "./sitemap.php";
	include "./login.php";
	include "./footer.php";
	class Viewpage
	{
		private $AuthRequired = null;
		private $AuthReturn = null;
		private $linking = null;
		private $Table = null;
		private $title = null;
		function Viewpage($AuthRequired, $AuthReturn, $linking, $Table, $title = null)
		{
			$this->AuthRequired = $AuthRequired;
			$this->AuthReturn = $AuthReturn;
			$this->linking = $linking;
			$this->Table = $Table;
			$this->title = $title;
		}
		function render()
		{	
			//Initialize SQL database object
			$SQL = new SQL();
			$Auth = new Authentication($SQL->Get_TABLE(1));
			
			if($this->AuthRequired&&!(($Auth->isAuthenticated())&&($Auth->hasPermissions(AUTH_LEVEL_WRITER))))
				header("location: ".$this->AuthReturn);
			//Get Basic Site Info
			if($this->title==null)
				$site_title = $SQL->GET_SITE_TITLE();
			else
				$site_title = $this->title;
			$site_logo = $SQL->GET_SITE_LOGO();
			//Get ID from HTML GET REQUEST or set to 0
			$page_id = 0;
			if(isset($_GET["id"]))
				$page_id = $_GET["id"];
			
			//Identify special pages and get page info;
			$page_title = null;
			$page_desc = null;
			$page_img = null;
			$page_src = null;
			switch ($page_id)
			{
				case -3:
					$Auth->TerminateUser();
					break;
				case -2:
					$page_title = "Login";
					break;
				case -1:
					$page_title = "Sitemap";
					break;
				default:
					$page_title = $SQL->GET_TABLE($this->Table)->GET_ROW($page_id)->GET_ATTRIBUTE(2);
					$page_desc = $SQL->GET_TABLE($this->Table)->GET_ROW($page_id)->GET_ATTRIBUTE(3);
					$page_img = $SQL->GET_TABLE($this->Table)->GET_ROW($page_id)->GET_ATTRIBUTE(4);
					$page_src = $SQL->GET_TABLE($this->Table)->GET_ROW($page_id)->GET_ATTRIBUTE(5);
					break;
			}
			
			//Initialize page component objects
			$css = new CSS();
			$header = new Header($site_title, $site_logo, $Auth, $this->linking);
			$nav = new NAV($SQL->GET_TABLE($this->Table), $this->linking, $page_id);
			$title = new Title($page_id, $page_title, $page_desc, $page_img);
			$breadcrumb = new Breadcrumb($SQL->GET_TABLE($this->Table), $this->linking, $page_id, $page_title);
			$body = new Body($page_src);
			$sitemap = new Sitemap($SQL->GET_TABLE($this->Table), $this->linking);
			$login = new Login($SQL->GET_TABLE(1), $Auth, "./".$this->linking);
			$footer = new footer($Auth, $this->linking);
			
			//render HTML output
?>
<html>
<head>
	<?php $css->render();?>
</head>
<body>
	<div id="webpage" class="center">
		<div id="navigation">
			<?php $header->render(); ?>
			<?php $nav->render(); ?>
		</div>
		<div id="content">
<?php
				$title->render();
				$breadcrumb->render();
				switch ($page_id)
				{
					case -2:
						$login->render();
						break;
					case -1:
						$sitemap->render();
						break;
					default:
						$body->render();
						break;
				}
?>
		</div>
		<?php $footer->render(); ?>
	</div>
</body>
</html>
<?php
		}
	}
?>