<?php
	//create footer
	class Footer
	{
		private $Auth = null;
		private $link = null;
		function Footer($Auth, $link)
		{
			$this->Auth = $Auth;
			$this->link = $link;
		}
		//render footer
		public function render()
		{
?>
			<footer id="footer">
				<div class="footerText left">
					<ul>
						<li>
<?php
							if($this->Auth->isAuthenticated())
							{
								echo $this->Auth->getUser();
?>					
								<ul>
									<?php if($this->Auth->hasPermissions(AUTH_LEVEL_WRITER)) { ?>
										<li><a class="footerLink" href="admin.php">Admin</a></li>
									<?php } ?>
									<li><a class="footerLink" href="<?php echo ($this->link."?id=-3") ?>">Log Out</a></li>
								</ul>						
<?php
							}
							else
							{
?>
								<a class="footerLink" href="<?php echo ($this->link."?id=-2") ?>">Log In</a>
<?php
							}
?>
						</li>
						<li><a class="footerLink" href="<?php echo $this->link;?>?id=-1">Site Map</a></li>
					</ul>
				</div>
			</footer>
<?php
		}
	}
?>