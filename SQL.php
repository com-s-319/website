<?php
	class SQL_ROW
	{
		private $data = null;
		public function SQL_ROW($data)
		{
			$this->data = $data;
		}
		public function GET_ATTRIBUTE($attr)
		{
			return $this->data[$attr];
		}
		public function SET_ATTRIBUTE($attr, $val)
		{
			$this->data[$attr] = $val;
		}
	}
	class SQL_TABLE
	{
		private $rows = null;
		function SQL_TABLE($src)
		{
			$this->rows = $src;
		}
		public function GET_ROW($id)
		{
			if(($id !== null) && ($id > -1) && ($id < count($this->rows)))
				return $this->rows[$id];
			return new SQL_ROW(null);
		}
		public function GET_ROWS_BY_ATTRIBUTE($attr, $id)
		{
			$i=0;
			foreach($this->rows as $row)
			{
				if(($row->GET_ATTRIBUTE($attr)==$id))
				{
					$Query[$i]=$row;
					$i++;
				}
			}
			if(!isset($Query))
				return null;
			return $Query;
		}
		public function GET_FIRST_ROW_BY_ATTRIBUTE($attr, $id)
		{
			return $this->GET_ROWS_BY_ATTRIBUTE($attr, $id)[0];
		}
		public function GET_ROWS()
		{
			return $this->rows;
		}
	}
	class SQL
	{
		private $tables = null;
		function SQL()
		{
			include "./fakeSQL.php";
			$this->tables[0] = new SQL_TABLE(getPages());
			$this->tables[1] = new SQL_TABLE(getUsers());
			$this->tables[2] = new SQL_TABLE(getAdmin());
		}
		public function GET_SITE_TITLE()
		{
			return "My Website";
		}
		public function GET_SITE_LOGO()
		{
			return "./logo.png";
		}
		public function GET_TABLE($id)
		{
			return $this->tables[$id];
		}
		//		ID	PID	Title	Description				Image			Content	

		//		Name		Password (Hashed)		AccessLevel
	}
?>