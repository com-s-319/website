<?php
	function getFile($location)
	{
		$myfile = fopen($location,"r");
		return fread($myfile,filesize($location));
	}
	function getPages()
	{
		return 
			[new SQL_ROW([0,-1,"Home"			, null							, null				,"This is the homepage"										]),
			 new SQL_ROW([1, 0,"Page1"			,"Page 1 Description"			, "./title.jpg"		,"This is page 1 html content"								]),
			 new SQL_ROW([2, 0,"Page2"			,"Page 2 Description"			, "./title.jpg" 	,"This is page 2 html content</br><button>Logon</button>"	]),
			 new SQL_ROW([3, 0,"Page3"			,"Page 3 Description"			, "./title.jpg" 	,"This is page 3 html content"								]),
			 new SQL_ROW([4, 0,"Page4"			,"Page 4 Description"			, "./title.jpg" 	,"This is page 4 html content"								]),
			 new SQL_ROW([5, 3,"Page5"			, null							, null				,"This is SUB page 5 html content"							]),
			 new SQL_ROW([6, 3,"Page6"			, null							, null				,"This is SUB page 6 html content"							]),
			 new SQL_ROW([7, 3,"Page7"			, null							, null				,"This is SUB page 7 html content"							]),
			 new SQL_ROW([8, 6,"Page8" 			, null							, null				,"This is SUB SUB page 8 html content"						])
			];
	}
	function getUsers()
	{
		return
			[new SQL_ROW(["Ryan",hash('sha256',"password"),AUTH_LEVEL_ADMIN]),
			 new SQL_ROW(["Amanda",hash('sha256',"password"),AUTH_LEVEL_NONE]),
			 new SQL_ROW(["Martin",hash('sha256',"password"),AUTH_LEVEL_ADMIN])
			];
	}
	function getAdmin()
	{
		return 
			[new SQL_ROW([0,-1,"Home"			, null			, null		,"This is the homepage"										]),
			 new SQL_ROW([1, 0,"Pages"			, null			, null		,"This is page 1 html content"								]),
			 new SQL_ROW([2, 0,"Users"			, null			, null 		,"This is page 2 html content</br><button>Logon</button>"	]),
			 new SQL_ROW([3, 0,"Site Info"		, null			, null 		,"This is page 3 html content"								])
			];
	}
	function getDrafts()
	{
		
	}
?>