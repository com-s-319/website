<?php
	//create sitemap
	class Sitemap
	{
		private $SQL_TABLE = null;
		private $link = null;
		function Sitemap($SQL_TABLE, $link)
		{
			$this->SQL_TABLE = $SQL_TABLE;
			$this->link = $link;
		}
		
		//recursive function to build sitemap hierarchy
		private function createSitemap($level)
		{
			//Base case: if the level is empty
			if(!isset($level))
				return "";
			//Create an unordered list for the level
			$sitemap = "\r\n<ul>\r\n";
			//populate list with elements
			foreach($level as $page)
			{
				$id=$page->GET_ATTRIBUTE(0);
				//Highlight the selected page.
				$title=$page->GET_ATTRIBUTE(2);
				//Add to sitemap
				$sitemap .= "<li><a href='".$this->link."?id=".$id."'>".$title."</a>";
				if($id != 0)
					$sitemap  .= $this->createSitemap($this->SQL_TABLE->GET_ROWS_BY_ATTRIBUTE(1,$id));
				$sitemap  .= "</li>\r\n";
			}
			$sitemap .= "</ul>\r\n";
			return $sitemap;
		}
		
		//render sitemap
		public function render()
		{
?>
			<div id="sitemap">
<?php
				$level = $this->SQL_TABLE->GET_ROWS_BY_ATTRIBUTE(1,0);
				//Add Home link
				array_unshift($level, $this->SQL_TABLE->GET_ROW(0));
				echo $this->createSitemap($level); ?>
			</div>
<?php
		}
	}
?>